//
//  CollectionHeaderReusableView.m
//  OffDutyTime
//
//  Created by lzz on 2019/10/12.
//  Copyright © 2019 lzz. All rights reserved.
//

#import "CollectionHeaderReusableView.h"

@implementation CollectionHeaderReusableView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor yellowColor];

        _titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, frame.size.width/2, frame.size.height)];
        _titleLabel.text = @"";
        [self addSubview:_titleLabel];
        
        _clearBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _clearBtn.frame = CGRectMake(self.bounds.size.width-120, 15, 100, 40);
        [_clearBtn setTitle:@"清除本月" forState:UIControlStateNormal];
        [_clearBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        _clearBtn.layer.masksToBounds = YES;
        _clearBtn.layer.cornerRadius = 5;
        _clearBtn.layer.borderWidth = 1;
        _clearBtn.layer.borderColor = [UIColor grayColor].CGColor;
        [_clearBtn addTarget:self action:@selector(clear:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_clearBtn];
    }
    return self;
    
}

#pragma mark -- CollectionHeaderViewDelegate --
- (void)clear:(id)sender {
    if (self.headerDelegate&&[self.headerDelegate respondsToSelector:@selector(clearData)]) {
        [self.headerDelegate clearData];
    }
}

@end
