//
//  MyCollectionViewCell.h
//  OffDutyTime
//
//  Created by lzz on 2019/10/12.
//  Copyright © 2019 lzz. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) UIImageView *topImage;

@property (strong, nonatomic) UILabel *btmlabel;
@property (strong, nonatomic) UILabel *timelabel;

@end

NS_ASSUME_NONNULL_END
