//
//  ViewController.m
//  OffDutyTime
//
//  Created by lzz on 2019/10/12.
//  Copyright © 2019 lzz. All rights reserved.
//

// 在工程里手动创建一个.plist文件，把固定的内容写入，这个需要人工手动写入(工程里只可读取，不可以写入)

#define kScreenWidth [UIScreen mainScreen].bounds.size.width
#define kScreenHeight [UIScreen mainScreen].bounds.size.height

#define kLineSpacing 10
#define kLineNum 7

#import "ViewController.h"
#import "MyCollectionViewCell.h"
#import "UIPickerViewAlert.h"
#import "CollectionHeaderReusableView.h"


@interface ViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UIPickerViewAlertDelegate,CollectionHeaderViewDelegate>

@property (nonatomic,strong)UICollectionView *myCollectionView;

@property (nonatomic,strong)UIPickerViewAlert *myPickView;

@property (nonatomic,strong)NSMutableDictionary *dataDayDic;

@property (nonatomic,strong)NSArray *totalDayArr;

@property (nonatomic,assign)NSInteger selectDay; // 填写哪一天
@property (nonatomic,strong)NSString *avarageOffDutyTime;

@property (nonatomic,strong)NSString *filePath; // plistPath
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.selectDay = 0;
    self.avarageOffDutyTime = @"00:00";

    _totalDayArr = @[@"1",@"2",@"3",@"4",@"5",@"6",@"7",
                     @"8",@"9",@"10",@"11",@"12",@"13",@"14",
                     @"15",@"16",@"17",@"18",@"19",@"20",@"21",
                     @"22",@"23",@"24",@"25",@"26",@"27",@"28",
                     @"29",@"30",@"31"];

    // 持久化存储
    [self createPlistFile];
    
    [self setupView];
}

- (void) createPlistFile
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    // 获取沙盒 Document目录
    NSString *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    self.filePath = [path stringByAppendingPathComponent:@"myTimeDog.plist"];
    
    //判断文件是否存在
    if ([fileManager fileExistsAtPath:self.filePath] == NO) {
        // 不存在，则创建
        NSDictionary *dic2 = @{@"1":@"00:00",@"2":@"00:00",@"3":@"00:00",@"4":@"00:00",@"5":@"00:00",@"6":@"00:00",@"7":@"00:00",@"8":@"00:00",@"9":@"00:00",@"10":@"00:00",@"11":@"00:00",@"12":@"00:00",@"13":@"00:00",@"14":@"00:00",@"15":@"00:00",@"16":@"00:00",@"17":@"00:00",@"18":@"00:00",@"19":@"00:00",@"20":@"00:00",@"21":@"00:00",@"22":@"00:00",@"23":@"00:00",@"24":@"00:00",@"25":@"00:00",@"26":@"00:00",@"27":@"00:00",@"28":@"00:00",@"29":@"00:00",@"30":@"00:00",@"31":@"00:00"
                               };
        [dic2 writeToFile:self.filePath atomically:YES];
        
        _dataDayDic = [NSMutableDictionary dictionaryWithContentsOfFile:self.filePath];

    }else{
        // 存在 读取plist文件  这里的是字典
        _dataDayDic = [NSMutableDictionary dictionaryWithContentsOfFile:self.filePath];
        NSLog(@"_dataDayDic--%@",_dataDayDic);
    }
}


#pragma mark -- 计算平均下班时间 --
// 返回每个时间的分钟数
- (NSInteger)ppp:(NSString *)time
{
    NSArray *minteArr = [time componentsSeparatedByString:@":"];
    NSString *hoursStr = minteArr[0];
    NSString *minutesStr = minteArr[1];

    NSInteger hours = [hoursStr integerValue] - 18;
    NSInteger minutes = [minutesStr integerValue];

    NSInteger totalMinutes = hours*60 + minutes;
    
    return totalMinutes;
}

// 根据分钟返回下班时间 2:30
- (NSString *)transformOffDutyTime:(NSInteger)totalMinutes
{
    NSInteger hours = totalMinutes/60;
    NSInteger minutes = totalMinutes%60;
    
    NSInteger offHour = 18 + hours;
    NSInteger offMinute = 0 + minutes;
    
    return [NSString stringWithFormat:@"%ld:%02ld",offHour,offMinute];
}

#pragma mark -- lazy laod --
- (UIPickerViewAlert *)myPickView {
    if (!_myPickView) {
        _myPickView = [[UIPickerViewAlert alloc]initWithFrame:self.view.bounds];
        _myPickView.alterDelegate = self;
    }
    return _myPickView;
}

- (void)setupView
{
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    /**
     创建collectionView
     */
    _myCollectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 64, kScreenWidth, kScreenHeight-64) collectionViewLayout:layout];
    _myCollectionView.delegate = self;
    _myCollectionView.dataSource = self;
    _myCollectionView.backgroundColor = [UIColor lightGrayColor];
    /**
     注册item和区头视图、区尾视图
     */
    [_myCollectionView registerClass:[MyCollectionViewCell class] forCellWithReuseIdentifier:@"MyCollectionViewCell"];
    [_myCollectionView registerClass:[CollectionHeaderReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"MyCollectionViewHeaderView"];
    [_myCollectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"MyCollectionViewFooterView"];
    [self.view addSubview:_myCollectionView];
}

#pragma mark - UICollectionViewDelegate,UICollectionViewDataSource
/**
 分区个数
 */
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}
/**
 每个分区item的个数
 */
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _dataDayDic.count;
}
/**
 创建cell
 */
- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"MyCollectionViewCell";
    MyCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    NSString *key = _totalDayArr[indexPath.item];
    cell.btmlabel.text = key;
    cell.timelabel.text = _dataDayDic[key];

    return cell;
}
/**
 创建区头视图和区尾视图
 */
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    if (kind == UICollectionElementKindSectionHeader){
        CollectionHeaderReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"MyCollectionViewHeaderView" forIndexPath:indexPath];
        
        headerView.titleLabel.text = [NSString stringWithFormat:@"本月平均下班时间：%@",self.avarageOffDutyTime];
        headerView.headerDelegate = self;
        return headerView;
        
    }else if(kind == UICollectionElementKindSectionFooter){
        UICollectionReusableView *footerView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"MyCollectionViewFooterView" forIndexPath:indexPath];
        footerView.backgroundColor = [UIColor blueColor];
        UILabel *titleLabel = [[UILabel alloc]initWithFrame:footerView.bounds];
        titleLabel.text = [NSString stringWithFormat:@"第%ld区尾：只统计18:00～24:00的有效下班打卡时间",indexPath.section];
        [footerView addSubview:titleLabel];
        return footerView;
    }
    return nil;
}
/**
 点击某个cell
 */
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"点击了第%ld分item",(long)indexPath.item);
    self.selectDay = indexPath.item;
    [self.view addSubview:self.myPickView];
}
/**
 cell的大小
 */
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat itemW = (kScreenWidth-(kLineNum+1)*kLineSpacing)/kLineNum-0.001;
    return CGSizeMake(itemW, itemW);
}
/**
 每个分区的内边距（上左下右）
 */
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(kLineSpacing, kLineSpacing, kLineSpacing, kLineSpacing);
}
/**
 分区内cell之间的最小行间距
 */
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return kLineSpacing;
}
/**
 分区内cell之间的最小列间距
 */
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return kLineSpacing;
}
/**
 区头大小
 */
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    return CGSizeMake(kScreenWidth, 65);
}
/**
 区尾大小
 */
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    return CGSizeMake(kScreenWidth, 65);
}

#pragma mark -- UIPickerViewAlertDelegate --

- (void)pickView:(NSString *)hour minute:(NSString *)minute
{
    // 修改当天下班时间
    NSLog(@"hour--%@  minute--%@",hour,minute);
    NSString *key = _totalDayArr[self.selectDay];
    _dataDayDic[key] = [NSString stringWithFormat:@"%@:%@",hour,minute];
    
    // 同步写到plist
    [_dataDayDic writeToFile:self.filePath atomically:YES];
    
    // 计算平均下班时间
    NSInteger totalMinutes = 0;
    NSInteger availableDay = 0;

    for (NSInteger i=0; i<_totalDayArr.count; i++) {
        NSString *key = _totalDayArr[i];
        NSString *value =  _dataDayDic[key];

        // 有效的下班时间
        if (![value isEqualToString:@"00:00"]) {
            availableDay = availableDay+1;
            NSInteger temMinutes = [self ppp:value];
            totalMinutes = totalMinutes + temMinutes;
        }
    }
    
    NSInteger perTotalMintues = totalMinutes/availableDay;
    NSString *averageOffduty = [self transformOffDutyTime:perTotalMintues];
    NSLog(@"averageOffduty----%@",averageOffduty);
    self.avarageOffDutyTime = averageOffduty;
    
    [self.myCollectionView reloadData];

}

- (void)closePickView
{
    [self.myPickView removeFromSuperview];
}

#pragma mark -- CollectionHeaderViewDelegate --
- (void)clearData
{
    NSLog(@"clearData");
    NSDictionary *dic2 = @{@"1":@"00:00",@"2":@"00:00",@"3":@"00:00",@"4":@"00:00",@"5":@"00:00",@"6":@"00:00",@"7":@"00:00",@"8":@"00:00",@"9":@"00:00",@"10":@"00:00",@"11":@"00:00",@"12":@"00:00",@"13":@"00:00",@"14":@"00:00",@"15":@"00:00",@"16":@"00:00",@"17":@"00:00",@"18":@"00:00",@"19":@"00:00",@"20":@"00:00",@"21":@"00:00",@"22":@"00:00",@"23":@"00:00",@"24":@"00:00",@"25":@"00:00",@"26":@"00:00",@"27":@"00:00",@"28":@"00:00",@"29":@"00:00",@"30":@"00:00",@"31":@"00:00"
                           };
    [dic2 writeToFile:self.filePath atomically:YES];
    
    _dataDayDic = [NSMutableDictionary dictionaryWithContentsOfFile:self.filePath];
    
    self.avarageOffDutyTime = @"00:00";

    [self.myCollectionView reloadData];
}

@end
