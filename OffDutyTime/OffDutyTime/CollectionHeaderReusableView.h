//
//  CollectionHeaderReusableView.h
//  OffDutyTime
//
//  Created by lzz on 2019/10/12.
//  Copyright © 2019 lzz. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN


@protocol CollectionHeaderViewDelegate <NSObject>

- (void)clearData;

@end

@interface CollectionHeaderReusableView : UICollectionReusableView

@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UIButton *clearBtn;

@property (nonatomic,assign) id <CollectionHeaderViewDelegate> headerDelegate;


@end

NS_ASSUME_NONNULL_END
