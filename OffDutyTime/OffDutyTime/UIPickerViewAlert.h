//
//  UIPickerViewAlert.h
//  OffDutyTime
//
//  Created by lzz on 2019/10/12.
//  Copyright © 2019 lzz. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol UIPickerViewAlertDelegate <NSObject>

- (void)pickView:(NSString *)hour minute:(NSString *)minute;
- (void)closePickView;

@end

@interface UIPickerViewAlert : UIView

@property (nonatomic,assign) id <UIPickerViewAlertDelegate> alterDelegate;

@end

NS_ASSUME_NONNULL_END
