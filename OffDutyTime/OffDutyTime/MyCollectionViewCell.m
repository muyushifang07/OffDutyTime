//
//  MyCollectionViewCell.m
//  OffDutyTime
//
//  Created by lzz on 2019/10/12.
//  Copyright © 2019 lzz. All rights reserved.
//

#import "MyCollectionViewCell.h"

@implementation MyCollectionViewCell

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self)
    {
        self.topImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        self.topImage.layer.masksToBounds = YES;
        self.topImage.layer.cornerRadius = 5;
        self.topImage.backgroundColor = [UIColor redColor];
        [self.contentView addSubview:self.topImage];
        
        self.btmlabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, frame.size.width, frame.size.height-25)];
        self.btmlabel.textAlignment = NSTextAlignmentCenter;
        self.btmlabel.textColor = [UIColor whiteColor];
        self.btmlabel.font = [UIFont fontWithName:@"Verdana-Bold" size:18];
        [self.contentView addSubview:self.btmlabel];
        
        self.timelabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.btmlabel.frame), frame.size.width, 20)];
        self.timelabel.textAlignment = NSTextAlignmentCenter;
        self.timelabel.textColor = [UIColor whiteColor];
        self.timelabel.font = [UIFont systemFontOfSize:14];
        [self.contentView addSubview:self.timelabel];
    }
    
    return self;
}


@end
