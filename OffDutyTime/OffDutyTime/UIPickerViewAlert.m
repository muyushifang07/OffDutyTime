//
//  UIPickerViewAlert.m
//  OffDutyTime
//
//  Created by lzz on 2019/10/12.
//  Copyright © 2019 lzz. All rights reserved.
//

#define Margin_Left 50

#import "UIPickerViewAlert.h"

@interface UIPickerViewAlert ()<UIPickerViewDataSource,UIPickerViewDelegate>

@property (strong, nonatomic) UIPickerView *myPickerView;

@property (strong, nonatomic) NSArray *level1;//一级目录数据
@property (strong, nonatomic) NSMutableArray *level2;//二级目录数据
@end


@implementation UIPickerViewAlert

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        
        UIButton *closeBtn = [UIButton buttonWithType:UIButtonTypeContactAdd];
        closeBtn.frame = CGRectMake(self.bounds.size.width-120, 180, 80, 80);
        [closeBtn addTarget:self action:@selector(close:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:closeBtn];
        
        [self initData];
        [self addSubview:self.myPickerView];
        [self bringSubviewToFront:closeBtn];
    }
    return self;

}


- (void)initData {
    self.level2 = [NSMutableArray arrayWithCapacity:0];
    
    //获取一级数据
    self.level1 = @[@"00",@"18",@"19",@"20",@"21",@"22",@"23"];
    
    //获取二级数据
    for (NSInteger i=0; i<60; i++) {
        NSString *tem = [NSString stringWithFormat:@"%02ld",(long)i];
        [self.level2 addObject:tem];
    }
}

- (UIPickerView *)myPickerView {
    if (!_myPickerView) {
        _myPickerView = [[UIPickerView alloc]init];
        _myPickerView.frame = CGRectMake(Margin_Left, 200, self.bounds.size.width - 2*Margin_Left,300);
        _myPickerView.dataSource = self;
        _myPickerView.delegate = self;
        _myPickerView.showsSelectionIndicator = YES;
        _myPickerView.backgroundColor = [UIColor lightGrayColor];
    }
    return _myPickerView;
}

#pragma mark 实现UIPickerViewDataSource协议
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 2;
}


- (NSInteger) pickerView:(nonnull UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if(component == 0) {
        return [self.level1 count];
    } else {
        return [self.level2 count];
    }
}


#pragma mark 实现UIPickerViewDelegate协议
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if(component == 0) {
        return [self.level1 objectAtIndex:row];
    } else {
        return [self.level2 objectAtIndex:row];
    }
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {

    NSInteger component0Row =[self.myPickerView selectedRowInComponent:0];
    NSInteger component1Row =[self.myPickerView selectedRowInComponent:1];

    NSString *seletedP1 = [self.level1 objectAtIndex:component0Row];
    NSString *seletedP2 = [self.level2 objectAtIndex:component1Row];
    NSLog(@"seletedP1:%@=seletedP2:%@",seletedP1,seletedP2);
    [self didSelectRowTime:seletedP1 minute:seletedP2];
}


#pragma mark -- UIPickerViewAlertDelegate --
- (void)close:(id)sender {
    NSInteger row1 = [self.myPickerView selectedRowInComponent:0];
    NSInteger row2 = [self.myPickerView selectedRowInComponent:1];
    NSString *seleted1 = [self.level1 objectAtIndex:row1];
    NSString *seleted2 = [self.level2 objectAtIndex:row2];
    NSString *title = [[NSString alloc]initWithFormat:@"%@ | %@", seleted1, seleted2];
    NSLog(@"title----%@",title);
    
    if (self.alterDelegate&&[self.alterDelegate respondsToSelector:@selector(closePickView)]) {
        [self.alterDelegate closePickView];
    }
}

- (void)didSelectRowTime:(NSString *)hour minute:(NSString *)minute {
    if (self.alterDelegate&&[self.alterDelegate respondsToSelector:@selector(pickView:minute:)]) {
        [self.alterDelegate pickView:hour minute:minute];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
